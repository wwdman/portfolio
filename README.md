# Portfolio Site

This repository contains the source code for my portfolio site. The site lists my professional achievements, experience, skills & contacts. Additionally both the data science and developer projects I have completed are showcased.

## Getting Started


## Prerequisites

The below are instructions for spinning up a development environment with Docker

The development environment requires Docker (https://hub.docker.com/?overlay=onboarding). Head to the link to download the desktop client. Once downloaded confirm it is running with:
```
docker -v
```

## Installing Docker Development Environment
The Portfolio docker file can be build with the spun up with the below 

```
git clone git@gitlab.com:wwdman/portfolio.git
cd portfolio
docker build . -t ds-cv
```

Then once the image is built, it can be spun up and connected to the current directory with the below command. Note * replace `<PATH_OF_REPO>` with the full path of wherever you stored this repo
```
docker run -it -v $(pwd):/app -p 8080:8080 ds-cv
```

Once spun up the development site can be seen by navigating to (http://localhost:8080/). Any changes made to the code within the repo should be reflected on the development site. If not refresh the page.


## Deployment with GitLab CI
The site is deployed to AWS S3 for serverless hosting, the portfolio uses GitLab CI to auto test & deploy to the a S3 bucket. To deploy simply commit to the master branch with the commit message `release-to-prod`

### Built With

* [Vue.js](https://vuejs.org/) - The frontend framework for the SPA

## Authors

* **Daniel Stratti** 


## Acknowledgments

* README.md Template: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2