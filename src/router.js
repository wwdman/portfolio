import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';


import About from './views/About.vue';
import Skills from './views/Skills.vue';
import Experiences from './views/Experiences.vue';
import Projects from './views/Projects.vue';
import Contact from './views/Contact.vue';


Vue.use(Router)

export default new Router({
    mode: 'history',

  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: About//() => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/skills',
      name: 'skills',
      component: Skills //() => import(/* webpackChunkName: "about" */ './views/Skills.vue')
    },
    {
      path: '/experiences',
      name: 'experiences',
      component: Experiences //() => import(/* webpackChunkName: "about" */ './views/Experiences.vue')
    },
    {
      path: '/projects/:type',
      name: 'projectsType',
      component: Projects //() => import(/* webpackChunkName: "about" */ './views/Projects.vue')
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects //() => import(/* webpackChunkName: "about" */ './views/Projects.vue')
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact //() => import(/* webpackChunkName: "about" */ './views/Contact.vue')
    },
    {
      path: '/paper',
      name: 'paper',
      component: () => import(/* webpackChunkName: "about" */ './components/layout/PaperDisplay.vue')
    },
  ],

  /* https://router.vuejs.org/en/advanced/scroll-behavior.html */
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    }


  
})