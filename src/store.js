import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  }
})


/*
import { 
    getLocalUser, 
    getLocalQuestionnaire, 
    getLocalQuestionnaires 
} from "./helpers/auth";

const user = getLocalUser();
const localQuestionnaire = getLocalQuestionnaire();
const localQuestionnaires = getLocalQuestionnaires();

console.log(user);

export default {
    / * Globally required data about the application * /
    state: {
        
    },

    / * Accessors * /
    getters: {
        
    },

    / * Synchronous and Committable methods to manipulate the store * /
    mutations: {

    },

    / * Asynchronous actions that will use a mutation * /
    actions: {

    }
};
*/