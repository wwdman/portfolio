export default {
    lang: [
        {
            key: "cpp",
            img: require('../logos/lang/icons8-c++.svg'),
            count: 3,
            url: "projects/cpp"
        }, {    
            key: "c",
            img: require('../logos/lang/icons8-c-programming.svg'),
            count: "",
            url: "projects/c"
        }, {    
            key: "csh",
            img: require('../logos/lang/icons8-c-sharp-logo.svg'),
            count: "",
            url: "projects/csh"
        }, {    
            key: "js",
            img: require('../logos/lang/icons8-javascript.svg'),
            count: 5,
            url: "projects/js"
        },  {    
            key: "php",
            img: require('../logos/lang/php-svg-to-png-3.png'),
            count: 5,
            url: "projects/php"
        }, {    
            key: "py",
            img: require('../logos/lang/icons8-python.svg'),
            count: "",
            url: "projects/py"
        }, {    
            key: "html",
            img: require('../logos/lang/icons8-html-5.svg'),
            count: 7,
            url: "projects/html"
        }, {    
            key: "css",
            img: require('../logos/lang/icons8-css3.svg'),
            count: 7,
            url: "projects/css"
        }, {    
            key: "r",
            img: require('../logos/lang/724px-R_logo.svg.png'),
            count: "",
            url: "projects/r"
        },{    
            key: "typescript",
            img: require('../logos/lang/typescript.png'),
            count: 3,
            url: "projects/angular"
        }, {    
            key: "java",
            img: require('../logos/lang/icons8-java.svg'),
            count: "",
            url: "projects/java"
        }, {    
            key: "sql",
            img: require('../logos/lang/sql.png'),
            count: 2,
            url: "projects/sql"
        },{    
            key: "bash",
            img: require('../logos/lang/bash.svg'),
            count: 3,
            url: "projects/bash"
        },
        /*{    
            key: "swift",
            img: require('../logos/lang/icons8-swift.svg'),
            count: 0,
            url: "projects/bash"
        }*/
    ],

    frame: [
        {    
            key: "wordpress",
            img: require('../logos/frame/icons8-wordpress.svg'),
            count: "",
            url: "projects/"
        },{    
            key: "laravel",
            img: require('../logos/frame/laravel-1-logo-svg-vector.svg'),
            count: "",
            url: "projects/"
        },{    
            key: "vuejs",
            img: require('../logos/frame/vuejs-seeklogo.com.svg'),
            count: "",
            url: "projects/"
        },{    
            key: "asp",
            img: require('../logos/frame/dot_net_core.png'),
            count: "",
            url: "projects/"
        },{    
            key: "bootstrap",
            img: require('../logos/frame/icons8-bootstrap.svg'),
            count: "",
            url: "projects/"
        },{
            key: "angular",
            img: require('../logos/frame/angular.png'),
            count: "",
            url: "projects/"
        },{
            key: "nodejs",
            img: require('../logos/frame/nodejs.png'),
            count: "",
            url: "projects/"
        }
    ],

    tech: [
        /*{
            key: "knime",
            img: require("../logos/tech/KNIME.png"),
            count: "",
            url:"projects/"
        },/ *
        {    
            key: "rpi",
            img: require('../logos/tech/icons8-raspberry-pi.svg'),
            count: "",
            url: "projects/"
        },*/
        {    
            key: "mongo",
            img: require('../logos/tech/mongodb.png'),
            count: "",
            url: "projects/"
        },
        {    
            key: "git",
            img: require('../logos/tech/icons8-git.svg'),
            count: "",
            url: "projects/"
        },/*{    
            key: "debian",
            img: require('../logos/tech/debian.png'),
            count: "",
            url: "projects/"
        },{    
            key: "kali",
            img: require('../logos/tech/kali.png'),
            count: "",
            url: "projects/"
        },{    
            key: "centos",
            img: require('../logos/tech/centos.png'),
            count: "",
            url: "projects/"
        },* /{    
            key: "arduino",
            img: require('../logos/tech/arduino.png'),
            count: "",
            url: "projects/"
        },*/{    
            key: "aws",
            img: require('../logos/tech/aws.png'),
            count: "",
            url: "projects/"
        },{    
            key: "docker",
            img: require('../logos/tech/docker.png'),
            count: "",
            url: "projects/"
        },{    
            key: "ubuntu",
            img: require('../logos/tech/ubuntu.png'),
            count: "",
            url: "projects/"
        },{    
            key: "cisco",
            img: require('../logos/tech/cisco.png'),
            count: "",
            url: "projects/"
        }
    ]
}