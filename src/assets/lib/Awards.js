export default [
    {
        title: "Cisco Systems Academic Achievement Award in Computer Networking",
        img: require("../logos/tech/cisco.png"),
        url: "/cisco_award.pdf",
    },{
        title: "Golden Key Honor Society",
        img: require("../certificates/gk-new-logo.gif"),
        url: "https://www.goldenkey.org/",
    },{
        title: "HIKM Paper 2018",
        img: require("../logos/outline-insert_drive_file-24px.svg"),
        url: "/HIKM_paper.pdf",
    },{
        title: "PASS Facilitator Certificate",
        img: require("../logos/outline-insert_drive_file-24px.svg"),
        url: "/pass_facilitator.pdf",
    }
]