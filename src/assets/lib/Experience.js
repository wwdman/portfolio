export default [
        {
            pos: "DEVELOPER ANALYST",
            org: "WESTERN SYDNEY UNIVERSTY",
            start: "2019",
            end: "2019",
            desc: "Employed as a developer analyst to identify business requirements and implement digital signage and web based solutions",
            duties: [
                "Developing workflows",
                "Designing user interfaces",
                "Developing Code",
                "Deploying and documenting systems",
            ]
        },{
            pos: "INTERN - PROCESS ANALYST",
            org: "WESTERN SYDNEY UNIVERSTY",
            start: "2019",
            end: "2019",
            desc: "Employed as a process analyst to aid in adapting current external processes to the universities student placement system.",
            duties: [
                "Conducting Interviews",
                "Developing workflows",
                "Adapt current processes to newly adopted systems",
                "Compare and recommend available solutions",
            ]
        },
        {
            pos: "PROGRAMMER",
            org: "WESTERN SYDNEY UNIVERSTY",
            start: "2018",
            end: "2018",
            desc: "Employed as a programmer for a 3-month project to implement a PHD candidates Health Information Staging System (HISS). HISS allows Clinicians to develop questionnaires and assign resources so that the most beneficial information is delivered to a participant, avoiding information overload and increasing their likelyhood to take action",
            duties: [
                "Design of the Database",
                "Development of the Laravel backend and RESTful API",
                "Design and Development of the Vue JS frontend, delivered as a Single Page Application.",
            ]
        },
/*
        {
            pos: "DIRECTOR",
            org: "WORKWEAR ONE",
            start: "2018",
            end: "2018",
            desc: "An ecommerce based uniform and custom apparel store that dynamically prices its clients based on artwork, garments and the quantities selected.",
            duties: [
                "Web design and development",
                "Procurement and negotiations of supply chains",
                "General administrative duties",
            ]
        },

        {
            pos: "PARTNER",
            org: "SORTED MARKETING",
            start: "2017",
            end: "2017",
            desc: "A marketing partnership specialising in the areas of graphic design, web development, digital marketing and promotional products procurement. Sorted Marketing was created to aid organisations in traversing the modern digital landscape and further the reach of their brand.",
            duties: [
                "Web design and development",
                "Procurement and negotiations of supply chains and merchandise",
                "General administrative duties",
            ]
        },
*/
        {
            pos: "SOLE TRADER",
            org: "DANIEL STRATTI",
            start: "2017",
            end: "2017",
            desc: "Services offered included building custom websites, Implementing CMS as well as performing optimizations for performance and search engine ranks",
            duties: [
                "Implementing to a CMS",
                "Meetings and Handovers",
                "Content Creation",
                "Image optimizations",
            ]
        },

        
        {
            pos: "PASS FACILLITATOR",
            org: "WESTERN SYDNEY UNIVERSTY",
            start: "2017",
            end: "2017",
            desc: "Peer Assisted Study Session (PASS) facilitator for the weekly face to face and online sessions for the units listed below. The goal of PASS is to create a collaborative learning environment and facilitate students to help each other better understand the concepts and content covered by a unit.",
            duties: [
                "Facilitating for Programming Techniques (face to face and online) during spring semester",
                "Facilitating for Programming Fundamentals (face to face) during autumn semester",
                "Preparing collaborative tasks related to unit content and skills of the students participating",
                "Facilitating sessions to be a comfortable, approachable and collaborative environment",
                "Guide questions in ways to promote self-learning of concepts.",
            ]
        },

        {
            pos: "ADMINISTRATOR",
            org: "STRATTI BUILDING GROUP",
            start: "2016",
            end: "CURRENT",
            desc: "Stratti Building Group is a construction company, specialising in high end architectural homes, which service the greater Sydney area.",
            duties: [
                "Maintaining and creating physical and digital records for accounts and personnel",
                "Payroll and Payments of all employees and trades",
                "General administration duties",
                "Creation of various scripts to automate repetative tasks",
            ]
        },
/*                    
        {
            pos: "TEAM MEMBER",
            org: "GRILL’D",
            start: "2016",
            end: "2016",
            desc: "A Burger restaurant located at Macarthur square, Campbelltown.",
            duties: [
                "Greeting and serving customers",
                "Cleaning and ensuring food safety",
                "Cooking, seasoning and presenting the chip selection",
            ]
        },

        {
            pos: "VOLUNTEER",
            org: "VOLUNTEER SOUTH AFRICA / GLENN AFRIC",
            start: "2015",
            end: "2015",
            desc: "A wildlife sanctuary home to many orphaned and injured animal including elephants, lions, tigers, cheetahs and many other South African species in need of care.",
            duties: [
                "Stable maintenance for horses and elephants",
                "Farm maintenance",
                "Animal enclosure maintenance",
                "Assist in training and exercise programs for animals",
            ]
        },

        {
            pos: "LEADING HAND",
            org: "MARDINI CONSTRUCTION",
            start: "2014",
            end: "2014",
            desc: "An architectural construction company working on major heritage projects and residential homes. I was employed as a Leading Hand for the construction of two architectural residences in Sydney’s McMahons Point.",
            duties: [
                "Setting out sites per site drawings",
                "Independently completing duties in a timely manner",
                "Supervision of works and staff when acting supervisor was off site",
            ]
        },
        {
            pos: "LEADING HAND",
            org: "HANNAS CIVIL ENGINEERING",
            start: "2013",
            end: "2014",
            desc: "A civil construction company working for major industries, various commercial clients and government agencies. I was employed as a leading hand for the construction of an electrical substation in Rookwood.",
            duties: [
                "Setting out sites per site drawings",
                "Supervising excavations, backfilling and labour works",
                "Reporting to supervisor on works completed",
                "Delegating daily duties to labour staff",
            ]
        },
        
        
        {
            pos: "LABOURER",
            org: "HANNAS CIVIL ENGINEERING",
            start: "2011",
            end: "2012",
            desc: "A civil construction company working for major industries, various commercial clients and government agencies. I was employed as a labourer to work on the construction of an architectural home in Sydney’s Hunters Hill",
            duties: [
                "Carrying out works alongside trades people in carpentry, tiling, stone masonry and form works",
                "Maintaining housekeeping",
                "Independently completing duties in a timely manner",
                "Aiding in site set out and reading site plans",
            ]
        },
        {
            pos: "VOLUNTEER",
            org: "FOOD WATER SHELTER",
            start: "2008",
            end: "2008",
            desc: "This is an Australian non-profit charity organization that established and built sustainable living village in Tanzania East Africa for orphaned children. In late 2008 I worked as a volunteer aiding to complete the construction of multi-story buildings including: accommodation building, amenities block and common building. These buildings went on to win the 2010 National Award for International Architecture from the Australian Institute of Architects.",
            duties: [
                "Constructing A-frames of roofing",
                "Cladding interior and exterior walls",
                "Communicating and completing tasks with Tanzanians that had little to no English experience",
                "Independently completing duties in a timely manner",
                "Aiding in site set out and reading of site plans"
            ]
        },
        {
            pos: "PIZZA CHEF / WAITER",
            org: "PRONTO PIZZA",
            start: "2007",
            end: "2011",
            desc: "A Pizzeria restaurant in Camden. I was employed firstly as a kitchen hand / waiter, and quickly progressed to pizza chef whilst still continuing waiter duties.",
            duties: [
                "Pizza and Pasta Chef",
                "Stock management and upkeep",
                "Customer service over the phone and in person",
                "General housekeeping",
            ]
        }
*/
]