export default [
    {
        title: "Applications of Big Data",
        short: "ABD",
        grade: "H",
        level: 3,
        mark: 93,
        semester: "Spring",
        year: 2019,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=301110.1",
        desc: "<h5>About this Unit</h5>\
        <p>Many techniques and tools have been developed over the past decade to cope with the ever-growing needs for the processing and analysis of big data. This unit will cover the key techniques that have been widely used in big data applications, such as relational and Not Only Structured Query Language (NoSQL) databases, Web Services, parallel and cloud computing, MapReduce, Hadoop and its eco-system. It aims to introduce the emerging technologies and applications in big data to students, and keep up with the latest trends in the industry.</p>",
        badges: [
            require("../logos/tech/mongodb.png"),
            require("../logos/tech/hadoop.png"),
            require('../logos/lang/icons8-python.svg')
        ]
    },
    {
        title: "Physics 1",
        short: "Physics1",
        grade: "H",
        level: 1,
        mark: 91,
        semester: "Autumn",
        year: 2019,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300828.1",
        desc: "<h5>About this Unit</h5>\
        <p>This unit provides an introduction to physics for science and medical science students as well as providing \
        a basis for further study of more advanced physics for students pursuing courses in nanotechnology, chemical, \
        physical and mathematical sciences. It provides a foundation to understand the physical principles which underlay \
        scientific instrumentation and analysis Topics covered include systems of units; \
            <ol>\
                <li>Introductory mechanics, Newtons laws, work, conservation of energy and momentum;</li>\
                <li>Electricity, electrostatics, DC and AC circuits and components, introductory electromagnetism;</li> \
                <li>Waves and optics, electromagnetic radiation, reflection, refraction, image formation, polarisation, interference and diffraction.</li>\
            </ol>\
        </p>",
        badges: [
            require("../svg_icon/twotone-offline_bolt-24px.svg"),
        ]
    },{
        title: "Professional Experience",
        short: "PX",
        grade: "H",
        level: 3,
        mark: 95,
        semester: "Autumn",
        year: 2019,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300579.6",
        desc: "<h5>About this Unit</h5> \
        <p>Professional Experience is a final year 'capstone' project unit. This unit provides opportunities for students to gain hands-on experience in software systems requirements definition, analysis, design and implementation, in a real-world setting. Students work in groups, guided by an academic supervisor or an industry mentor, in achieving the goals set by the client that provides the project. Suitable projects are sourced from external organisations or within Western Sydney University by way of giving the students professional experience in independent learning and reflective practice</p>",
        badges: [
            require('../logos/frame/angular.png'),
            require('../logos/frame/nodejs.png'),
            require('../logos/frame/expressjs.png'),
        ]
    },{
        title: "Thinking About Data",
        short: "TAD",
        grade: "H",
        level: 1,
        mark: 94,
        semester: "Autumn",
        year: 2019,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=301108.1&ulocode=PAR",
        desc: "<h5>About this Unit</h5> \
        <p>This Unit covers basic concepts of data centric thinking. The main areas discussed are; Populations and Samples; Sampling concepts; Types of Data; Descriptive Methods; Estimation and Inference; Modelling. The Unit takes a computational and nonparametric approach, before briefly discussing theoretical concepts and distribution theory.</p>",
        badges: [
            require("../logos/lang/724px-R_logo.svg.png"),
            require('../logos/tech/tidyverse-ggplot2.png')
        ]
    },{
        title: "Introduction to Data Science",
        short: "IDS",
        grade: "H",
        level: 3,
        mark: 92,
        semester: "Autumn",
        year: 2019,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=301033.1&ulocode=PAR",
        desc: "<h5>About this Unit</h5> \
        <p>Analysis of data is essential for scientific investigation, modelling processes and predicting future events. Data Science is the investigation of the tools required that allow us to perform this modelling and prediction. The increase in accessible data over the past few decades has promoted the use of Data Science, making it a desired skill in many professions. In this unit we further investigate the methods of regression, clustering and classification that form the basis of a data scientist’s toolbox.</p>",
        badges: [
            require("../logos/lang/724px-R_logo.svg.png"),
            require('../logos/tech/tidyverse-ggplot2.png')
        ]
    },{
        title: "Professional Development",
        short: "PD",
        grade: "H",
        level: 3,
        mark: 95,
        semester: "Summer",
        year: 2019,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300578.3&ulocode=PAR",
        desc: "<h5>About this Unit</h5> \
        <p>This is a final year unit that builds on foundation and intermediate computing units to prepare students for professional experience. The unit covers the content in three modules as \
            <ol>\
                <li>Ethics and Professional Code of Conduct,</li>\
                <li>Project Management, and</li>\
                <li>Legal, Social, Environmental issues, Quality Assurance and IT Compliance.</li>\
            </ol>\
            The content covered in these three modules are carefully designed to fill in the gaps in knowledge that is not so far covered in previous units in preparing students for the challenging projects units and professional working life ahead. This unit is a pre-requisite to the capstone project, covered in Professional Experience Project unit.</p>",
        badges: [
            require("../svg_icon/baseline-bar_chart-24px.svg"),
        ]
    },{
        title: "Web Systems Development",
        short: "WSD",
        grade: "H",
        level: 3,
        mark: 95,
        semester: "Spring",
        year: 2018,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300583.3&ulocode=PEN",
        desc: "<h5>Assumed Knowledge</h5> \
        <p>\
        <ul><li>Fundamental web development skills such as HTML, CSS, Javascript and PHP.</li>\
            <li>Principles of relational database design and development, practical skills in SQL.</li></ul>\
        Principles of systems analysis and design including the specification of end-user requirements and a good knowledge of the SDLC and its application to solving computer system related problems.</p>\
        <h5>About this Unit</h5>\
        <p>In this unit students further develop their theoretical and practical skills in designing and developing web based information systems using systems analysis, programming, database, human computer interaction and web technologies skills that they have learnt in previous units. Current web development technologies and/or frameworks will be utilised to build a complex web information system in a collaborative web development team. Techniques of porting web systems to mobile platforms will also be explored.</p>",
        badges: [
            require('../logos/lang/icons8-c-sharp-logo.svg'),
            require('../logos/frame/dot_net_core.png'),
        ]
    },{
        title: "Social Web Analytics",
        short: "SWA",
        grade: "H",
        level: 3,
        mark: 90,
        semester: "Spring",
        year: 2018,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300958.2",
        desc: "<h5>Assumed Knowledge</h5> \
        <p>Students are expected to be familiar with fundamental computer programming concepts.</p>\
        <h5>About this Unit</h5>\
        <p>The Social Web provides everyone with a voice, information from Facebook, Twitter and Google+ should allow us to identify trends and relationships in society. Whilst this has interest on a personal level, the killer-apps will be in analyzing such data for business; tracking the buzz around a new product, understanding the links between customers etc. This unit will introduce its students to the Social Web data that is available, and blend computational, mathematical and statistical concepts to allow extraction and analysis of such data.</p>",
        badges: [
            require('../logos/lang/724px-R_logo.svg.png'),
        ]
    },
    {
        title: "Information Security",
        short: "IS",
        grade: "H",
        level: 3,
        mark: 90,
        semester: "Spring",
        year: 2018,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300128.5&ulocode=PEN",
        desc: "<h5>Assumed Knowledge</h5> \
        <p>Basic understanding of data structures, number theory and probability theory. Basic programming skills in C, C++, java, etc</p>\
        <h5>About this Unit</h5>\
        <p>This unit is concerned with the protection and privacy of information in computer systems. The focus of the course is primarily on introducing basic concepts in computer and information security and then using this knowledge as the vehicle to study the design and implementation of secure computer and network systems. This unit also provides students with practical experience with security programming. In more specific terms, the unit is intended to provide the following: Basic concepts of conventional and public key encryption; Number theory and its application in public key encryption and signatures; Protocols used in secure computer systems.</p>",
        badges: [
            require('../logos/lang/icons8-c++.svg'),
        ]
    },
    {
        title: "Network Security",
        short: "NS",
        grade: "D",
        level: 3,
        mark: 79,
        semester: "Spring",
        year: 2018,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300143.4&ulocode=SYD_CTY",
        desc: "<h5>Assumed Knowledge</h5> \
        <p>Good understanding of the principles of information security, and computer networks and internets.</p>\
        <h5>About this Unit</h5>\
        <p>This unit is concerned with the protection of information transferred over computer networks. It includes discussion of techniques for securing data transported over local and wide area networks. At the conclusion of the unit students will have a good understanding of the practical aspects of securing a computer network against internal and external attacks.</p>",
        badges: [
            require('../logos/lang/icons8-python.svg')
        ]
    },
    {
        title: "Systems Programming 1",
        short: "SP1",
        grade: "H",
        level: 2,
        mark: 92,
        semester: "Autumn",
        year: 2018,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300167.4&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5>\
        <p>This unit requires a knowledge base of at least the level of a completed first year in a professional Computing degree. Ability to apply fundamental concepts in data structures, algorithms, programming principles will be assumed.</p>\
        <h5>About this Unit</h5> \
        <p>This unit provides an introduction to the knowledge and skills required for the design, writing and support of technical software and other such functions normally falling within the role of the systems programmer. It provides for detailed study of a systems programming environment and its application to systems programming tasks.</p>",
        badges: [
            require('../logos/lang/icons8-c-programming.svg')
        ]
    },
    {
        title: "Human-Computer Interaction",
        short: "HCI",
        grade: "H",
        level: 3,
        mark: 88,
        semester: "Autumn",
        year: 2018,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300570.3&ulocode=CAM",
        desc: "<h5>About this Unit</h5> \
        <p>A key component to the discipline of Information Systems is the understanding and the advocacy of the user in the development of IT applications and systems. IT graduates must develop a mind-set that recognizes the importance of users and organisational contexts. They must employ user centered methodologies in the development, evaluation, and deployment of IT applications and systems. This unit examines human-computer interaction in order to develop and evaluate software, websites and information systems that not only look professional but are usable, functional and accessible.</p>",
        badges: [
            require('../logos/frame/vuejs-seeklogo.com.svg')
        ]
    },
    {
        title: "Operating Systems Programming",
        short: "OSP",
        grade: "H",
        level: 3,
        mark: 86,
        semester: "Autumn",
        year: 2018,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300698.4&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5>\
        <p>Students are expected to have a general understanding on computer systems; computer fundamentals, and programming techniques.</p>\
        <h5>About this Unit</h5> \
        <p>This unit provides the knowledge of the internal structure and functionality of Operating Systems. An operating system defines an abstraction of hardware behavior and provides a range of services more suitable for ICT application development than what raw hardware could deliver, in terms of convenience, efficiency and security. It is important that ICT Professionals have some understanding of how these services are realized. For ICT Professionals whose role includes supporting the operating system this unit provides the introduction to the relevant theory and practice.</p>",
        badges: [
            require('../logos/lang/icons8-c-programming.svg')
        ]
    },

    /* 2017 Autumn */
    {
        title: "Discrete Mathematics",
        short: "DM",
        grade: "C",
        level: 1,
        mark: 73,
        semester: "Autumn",
        year: 2017,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=200025.2&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5>\
        <p>HSC Mathematics or equivalent</p>\
        <h5>About this Unit</h5> \
        <p>This Level 1 unit introduces set theory, symbolic logic, graph theory and some counting problems. It provides a solid foundation for further study in mathematics or computing.</p>",
        badges: [
        require("../svg_icon/sharp-multiline_chart-24px.svg")
            
        ]
    },

    {
        title: "Data Structures and Algorithms",
        short: "DSA",
        grade: "H",
        level: 2,
        mark: 92,
        semester: "Autumn",
        year: 2017,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300103.4&ulocode=PEN",
        desc: "<h5>About this Unit</h5> \
        <p>This unit introduces students to fundamental data structures and algorithms used in computing. The material covered forms the basis for further studies in programming and software engineering in later units and for further training in programming skills. The unit focuses on the ideas of data abstraction and algorithm efficiency. The issues of computational complexity of algorithms are addressed throughout the semester. The topics covered include the fundamental abstract data types (lists, stacks, queues, trees, hash tables, graphs), recursion, complexity of algorithms, sorting and searching algorithms, binary search trees and graphs.</p>",
        badges: [
            require('../logos/lang/icons8-c++.svg')
        ]
    },

    {
        title: "Computer Networks and Internets",
        short: "CNI",
        grade: "H",
        level: 3,
        mark: 90,
        semester: "Autumn",
        year: 2017,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300095.4&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5>\
        <p>Fundamentals of data communications and computer networking, such as that covered in the prerequisite unit.</p>\
        <h5>About this Unit</h5> \
        <p>This unit extends on the work undertaken in the prerequisite unit and provides students with an in-depth explanation on the role of the architecture, components, and operations of routers and switches in a small network. Students will configure and troubleshoot routers and switches and resolve common issues with RIPv1, RIPng, single- area and multi-area OSPF, virtual LANs, and inter-VLAN routing in both IPv4 and IPv6 networks. This is the second of three units that prepares the student for industry based networking certification (CCNA).</p>",
        badges: [
            require('../logos/tech/cisco.png')
        ]
    },

    {
        title: "Object Oriented Analysis",
        short: "OOA",
        grade: "D",
        level: 2,
        mark: 83,
        semester: "Autumn",
        year: 2017,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300144.5&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5>\
        <p>General understanding of what an information system is and how information systems development is undertaken and •   Introductory knowledge about system analysis and design, including - basic problem solving experience in computerised information systems - ability to derive systems requirements from problem definitions - ability to produce system models using process, data, object and network modelling. - understanding design and implementation issues include, (but may not be limited to), elementary database design, input, output and user interface design and prototyping. • General knowledge on programming languages - Understanding difference between procedure programming and object oriented programming - Introductory knowledge of classes and objects and the class construction - Introductory knowledge on object orientation, including encapsulation, inheritance</p>\
        <h5>About this Unit</h5> \
        <p>The core strength of this unit is to analyse and model business objectives and critical requirements of software systems to be developed using object-oriented (OO) approaches. The system analysis is taken to greater depths within the context of Object Orientation. The Unified Modelling Language version 2.0 (notably use cases, user case diagrams, activity diagrams, class diagrams and sequence diagrams) is used as the modelling standard for creating OO models in the problem space. The unit also covers the rational unified process methodology and applications of design patterns for software development through practical case studies.</p>",
        badges: [
            require("../svg_icon/twotone-bubble_chart-24px.svg")
        ]
    },

    /* 2016 Summer */
    {
        title: "Technologies for Web Applications",
        short: "TWA",
        grade: "H",
        level: 2,
        mark: 100,
        semester: "Summer",
        year: 2017,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300582.5&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5>\
        <p>Basic programming principles and program control structures equivalent to that covered in Programming Fundamentals. Basic file management and PC operation including how to access and search the World Wide Web.</p>\
        <h5>About this Unit</h5> \
        <p>Building on material covered in Programming Fundamentals this unit introduces students to some of the key technologies for developing interactive and dynamic web applications from both the client and server perspective. The unit covers web site design, web site development, web page accessibility and usability, HTML, CSS, client side and server side scripting, database interaction, web site promotion (Search Engine Optimisation) and web security.</p>",
        badges: [
            require('../logos/lang/icons8-css3.svg'),
            require('../logos/lang/icons8-html-5.svg'),
            require('../logos/lang/icons8-javascript.svg'),
            require('../logos/lang/php-svg-to-png-3.png'),
        ]
    },

    /* 2016 Spring */
    {
        title: "Database Design and Development",
        short: "DDD",
        grade: "H",
        level: 2,
        mark: 96,
        semester: "Spring",
        year: 2016,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300104.4&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5>\
        <p>Basic programming skills, including variable declaration, variable assignment, selection statement and loop structure.</p>\
        <h5>About this Unit</h5> \
        <p>The main purpose of this unit is to provide students with an opportunity to gain a basic knowledge of database design and development including data modeling methods, techniques for database design using a set of business rules that are derived from a case study and finally implementation of the database using a commercial relational database management system. The unit also examines a number of important database concepts such as database administration, concurrency, backup and recovery and security. At the same time student learning and intercommunication skills are enhanced by running tutorial presentations and group assignments.</p>",
        badges: [
            require('../logos/lang/sql.png'),
        ]
    },
    {
        title: "Computer Networking",
        short: "CN",
        grade: "H",
        level: 2,
        mark: 96,
        semester: "Spring",
        year: 2016,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300565.2&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5>\
        <p>Fundamentals of computer architecture, binary and hexadecimal numbering systems, and programming principles. They should also have a working knowledge of the World Wide Web</p>\
        <h5>About this Unit</h5> \
        <p>Computer Networking is an introductory unit in computer systems networking. It covers basic networking technologies, Ethernet fundamentals, ISO OSI model, routing, switching and subnetting, the Internet architecture, networking protocols including TCP/IP, important OSI layer 2 and 3 networking device fundamentals, basic network management and security issues. This unit is also the first of three units, which will prepare students for industry based networking certification (CCNA).</p>",
        badges: [
            require('../logos/tech/cisco.png')
        ]
    },
    {
        title: "Programming Techniques",
        short: "PT",
        grade: "H",
        level: 2,
        mark: 95,
        semester: "Spring",
        year: 2016,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300581.4&ulocode=CAM",
        desc: "<h5>About this Unit</h5> \
        <p>This unit is intended as a second unit of study in programming. It builds on a basic understanding of procedural programming as would be developed in a first unit. This unit continues the development of programming skills and methodologies required for professional programming and for further study in later computing units. Topics covered include multi-dimensional arrays, file I/O, searching and sorting, and an introduction to object-oriented programming involving classes and inheritance.</p>",
        badges: [
            require('../logos/lang/icons8-java.svg'),
        ]
    },

    /* 2016 Autumn */
    {
        title: "Principles of Professional Communication 1",
        short: "PPC1",
        grade: "D",
        level: 1,
        mark: 75,
        semester: "Autumn",
        year: 2016,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=100483.2&ulocode=CAM",
        desc: "<h5>About this Unit</h5> \
        <p>The unit provides students with an introductory understanding of a range of communication theories and practices necessary for academic work and professional success.</p>",
        badges: [
            require("../svg_icon/twotone-chrome_reader_mode-24px.svg")
        ]
    },
    {
        title: "Programming Fundamentals",
        short: "PF",
        grade: "H",
        level: 1,
        mark: 97,
        semester: "Autumn",
        year: 2016,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300580.3&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5> \
        <p>High school mathematics at Year 10 level.</p>\
        <h5>About this Unit</h5> \
        <p>As a first unit in computer programming, Programming Fundamentals covers the basics of developing software with an emphasis on procedural programming. Students will learn about basic data structures, the concept of algorithms, fundamental programming constructs, common programming language features and functions, program design and good programming style. A high level programming language is combined with a highly visual framework to teach problem solving using software.</p>",
        badges: [
            require('../logos/lang/icons8-java.svg'),
        ]
    },
    {
        title: "Systems Analysis and Design",
        short: "SAD",
        grade: "D",
        level: 1,
        mark: 81,
        semester: "Autumn",
        year: 2016,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300585.2&ulocode=CAM",
        desc: "<h5>Assumed Knowledge</h5> \
        <p>Students should have knowledge of the fundamentals of information systems, computer systems, computer applications and information processing</p>\
        <h5>About this Unit</h5> \
        <p>This unit introduces the concepts of System Analysis and Design. The study of methodologies and techniques for problem recognition, requirement analysis, process modelling and/or data modelling are essential elements of this unit. The Systems Development Life Cycle model is employed as the prime approach to teach the unit, providing students with the basic skills required for analysis and design of logical solutions to information systems problems. The use of Computer Aided System Engineering tools will be discussed in practical sessions.</p>",
        badges: [
            require("../svg_icon/twotone-layers-24px.svg")
        ]
    },
    {
        title: "Statistical Decision Making ",
        short: "SDM",
        grade: "C",
        level: 1,
        mark: 72,
        semester: "Autumn",
        year: 2016,
        source: "http://handbook.westernsydney.edu.au/hbook/unit.aspx?unit=300700.6",
        desc: "<h5>About this Unit</h5> \
        <p>Statistical Decision Making introduces students to various statistical techniques supporting the study of computing and science. Presentation of the content will emphasize the correct principles and procedures for collecting and analysing scientific data, using information and communication technologies. Topics include describing different sets of data, probability distributions, statistical inference, and simple linear regression and correlation.</p>",
        badges: [
            require('../logos/lang/724px-R_logo.svg.png'),
        ]
    },
]