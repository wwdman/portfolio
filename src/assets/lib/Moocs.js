export default {
    langCerts: [
        {
            title: "C",
            img: require("../certificates/code/cert-c.jpg"),
            url: "",
            badges: []
        },{
            title: "C++",
            img: require("../certificates/code/cert-cpp.jpg"),
            url: "",
            badges: []
        },{
            title: "C#",
            img: require("../certificates/code/cert-csharp.jpg"),
            url: "",
            badges: []
        },{
            title: "Python",
            img: require("../certificates/code/cert-python.jpg"),
            url: "",
            badges: []
        },{
            title: "PHP",
            img: require("../certificates/code/cert-php.jpg"),
            url: "",
            badges: []
        },{
            title: "SQL",
            img: require("../certificates/code/cert-sql.jpg"),
            url: "",
            badges: []
        },{
            title: "JavaScript",
            img: require("../certificates/code/cert-js.jpg"),
            url: "",
            badges: []
        },{
            title: "HTML",
            img: require("../certificates/code/cert-html.jpg"),
            url: "",
            badges: []
        },{
            title: "CSS",
            img: require("../certificates/code/cert-css.jpg"),
            url: "",
            badges: []
        },
    ],

    frameWorkCert: [
        {
            title: "Vue JS",
            img: require("../certificates/code/LearningVuejs_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Angular",
            img: require("../certificates/code/LearningAngular_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Angular Routing",
            img: require("../certificates/code/Angular_Routing_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "NoSQL Databases",
            img: require("../certificates/code/LearningNoSQLDatabases_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "ASP .NET Core <abbr title='Model View Controller'>MVC</abbr>",
            img: require("../certificates/code/LearningASPNETCoreMVC_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "iOS AppDevelopment UI with Visual Tools",
            img: require("../certificates/code/iOSAppDevelopment_UIwithVisualTools_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Building Your First iOS App",
            img: require("../certificates/code/BuildingYourFirstiOSApp_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Blockchain Learning Solidity",
            img: require("../certificates/code/Blockchain_LearningSolidity_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Ethereum Building Blockchain <abbr title='Decentralized Apps'>DApps</abbr>",
            img: require("../certificates/code/Ethereum_BuildingBlockchainDecentralizedApps(DApps)_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },
    ],


    apiCerts: [
        {
            title: "Building RESTful APIs in Laravel",
            img: require("../certificates/api/BuildingRESTfulAPIsinLaravel_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Designing RESTful APIs",
            img: require("../certificates/api/DesigningRESTfulAPIs_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "HTTP Essential Training",
            img: require("../certificates/api/HTTPEssentialTraining_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Learning REST APIs",
            img: require("../certificates/api/LearningRESTAPIs_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "WebSecurity OAuth and OpenID Connect",
            img: require("../certificates/api/WebSecurity_OAuthandOpenIDConnect_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },
    ],

    analysisCerts: [
        {
            title: "Statistics Foundations 2",
            img: require("../certificates/analysis/StatisticsFoundations_2_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Statistics Foundations 3",
            img: require("../certificates/analysis/StatisticsFoundations_3_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "The Essential Elements of Predictive Analytics &amp; Data-Mining",
            img: require("../certificates/analysis/TheEssentialElementsofPredictiveAnalyticsandDataMining_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Big Data Foundations Techniques &amp; Concepts",
            img: require("../certificates/analysis/BigDataFoundations_TechniquesandConcepts_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Artificial Intelligence Foundations Thinking Machines",
            img: require("../certificates/analysis/ArtificialIntelligenceFoundations_ThinkingMachines_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Edge Analytics IoT &amp; Data Science",
            img: require("../certificates/analysis/EdgeAnalytics_IoTandDataScience_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Social Network Analysis Using R",
            img: require("../certificates/analysis/SocialNetworkAnalysisUsingR_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Business Analytics Foundations Predictive Prescriptive and Experimental Analytics",
            img: require("../certificates/analysis/BusinessAnalyticsFoundations_PredictivePrescriptiveandExperimentalAnalytics_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Machine Learning &amp; <abbr title='Artificial Intelligence'>AI</abbr> Foundations Classification Modeling",
            img: require("../certificates/analysis/MachineLearningandAIFoundations_ClassificationModeling_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Machine Learning &amp; <abbr title='Artificial Intelligence'>AI</abbr> Foundations Clustering &amp; Association",
            img: require("../certificates/analysis/MachineLearningandAIFoundations_ClusteringandAssociation_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },
    ],

    miscCerts: [
        {
            title: "Google: The Digital Garage",
            img: require("../certificates/Digital Garage Certificate.png"),
            url: "",
            badges: []
        },{
            title: "Solar Energy",
            img: require("../certificates/Edex.png"),
            url: "",
            badges: []
        },{
            title: "Blockchain Basics",
            img: require("../certificates/code/BlockchainBasics_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Blockchain Beyond the Basics",
            img: require("../certificates/code/Blockchain_BeyondtheBasics_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Learning Data Governance",
            img: require("../certificates/LearningDataGovernance_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "<abbr title='Internet of Things'>IoT</abbr> Foundations: Low-power Wireless Networking",
            img: require("../certificates/IoTFoundations_Low-PowerWirelessNetworking_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Smart Cities: Solving Uburan Problems Using Technology",
            img: require("../certificates/SmartCities_SolvingUrbanProblemsUsingTechnology_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Smart Cities: Using Data to Drive Urban Innovation",
            img: require("../certificates/SmarterCities_UsingDatatoDriveUrbanInnovation_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Learning Web Semantics",
            img: require("../certificates/LearningWebSemantics_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Open Data Unleashing Hidden Value",
            img: require("../certificates/OpenData_UnleashingHiddenValue_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Project Management Foundations",
            img: require("../certificates/ProjectManagementFoundations_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },{
            title: "Project Management Iterative Methods",
            img: require("../certificates/ProjectManagement_IterativeMethods_CertificateOfCompletion-1.png"),
            url: "",
            badges: []
        },
    ],

    awsCerts: [
        {
            title: "<abbr title='Amazon Web Services'>AWS</abbr> Essential Training",
            img: require("../certificates/aws/AmazonWebServicesEssentialTraining_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },
        {
            title: "<abbr title='Amazon Web Services'>AWS</abbr> Data Services",
            img: require("../certificates/aws/AmazonWebServices_DataServices_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },
        {
            title: "<abbr title='Amazon Web Services'>AWS</abbr> High Availability &amp; Elasticity",
            img: require("../certificates/aws/AWSforDevOps_HighAvailabilityandElasticity_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },
        {
            title: "<abbr title='Amazon Web Services'>AWS</abbr> for DevOps Monitoring Metrics &amp; Logging",
            img: require("../certificates/aws/AWSforDevOps_MonitoringMetricsandLogging_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },
        {
            title: "<abbr title='Amazon Web Services'>AWS</abbr> Monitoring &amp; Metrics",
            img: require("../certificates/aws/AmazonWebServices_MonitoringandMetrics_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },
        {
            title: "<abbr title='Amazon Web Services'>AWS</abbr> for Data Science",
            img: require("../certificates/aws/AmazonWebServicesforDataScience_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },
    ],

    securityCerts: [
        {
            title: "Cybersecurity for IT Professionals",
            img: require("../certificates/security/CybersecurityforITProfessionals_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Cyber Security Foundations",
            img: require("../certificates/security/CybersecurityFoundations_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Cybersecurity with Cloud Computing",
            img: require("../certificates/security/CybersecuritywithCloudComputing_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Implementing an Information Security Program",
            img: require("../certificates/security/ImplementinganInformationSecurityProgram_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "IT Security Foundations: Core Concepts",
            img: require("../certificates/security/ITSecurityFoundations_CoreConcepts_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "IT Security Foundations: Network Security",
            img: require("../certificates/security/ITSecurityFoundations_NetworkSecurity_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "IT Security Foundations: Operating System Security",
            img: require("../certificates/security/ITSecurityFoundations_OperatingSystemSecurity_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Learning Computer Forensics",
            img: require("../certificates/security/LearningComputerForensics_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Learning Computer Security Investigation and Response",
            img: require("../certificates/security/LearningComputerSecurityInvestigationandResponse_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Learning Cryptography and Network Security",
            img: require("../certificates/security/LearningCryptographyandNetworkSecurity_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Learning Secure Sockets Layer",
            img: require("../certificates/security/LearningSecureSocketsLayer_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "Symmetric Cryptography Essential Training",
            img: require("../certificates/security/SymmetricCryptographyEssentialTraining_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "<abbr title='Cisco Certified Network Associate'>CCNA</abbr> Security(210-260) Certification Preperation - <abbr title='Virtual Private Network'>VPN</abbr>",
            img: require("../certificates/security/CCNASecurity(210-260)CertPrep_3VPN_CertificateOfCompletion.png"),
            url: "",
            badges: []
        },{
            title: "<abbr title='System Security Certified Professional'>SSCP</abbr> Certification Preperation - Cryptography",
            img: require("../certificates/security/SSCPCertPrep_5Cryptography_CertificateOfCompletion.png"),
            url: "",
            badges: []
        }
    ],
    lang: [
        {
            key: "cpp",
            img: require('../logos/lang/icons8-c++.svg'),
            count: 3,
            url: "projects/cpp"
        }, {    
            key: "c",
            img: require('../logos/lang/icons8-c-programming.svg'),
            count: "",
            url: "projects/c"
        }, {    
            key: "csh",
            img: require('../logos/lang/icons8-c-sharp-logo.svg'),
            count: "",
            url: "projects/csh"
        }, {    
            key: "js",
            img: require('../logos/lang/icons8-javascript.svg'),
            count: 5,
            url: "projects/js"
        },  {    
            key: "php",
            img: require('../logos/lang/php-svg-to-png-3.png'),
            count: 5,
            url: "projects/php"
        }, {    
            key: "py",
            img: require('../logos/lang/icons8-python.svg'),
            count: "",
            url: "projects/py"
        }, {    
            key: "html",
            img: require('../logos/lang/icons8-html-5.svg'),
            count: 7,
            url: "projects/html"
        }, {    
            key: "css",
            img: require('../logos/lang/icons8-css3.svg'),
            count: 7,
            url: "projects/css"
        }, {    
            key: "r",
            img: require('../logos/lang/724px-R_logo.svg.png'),
            count: "",
            url: "projects/r"
        }, {    
            key: "java",
            img: require('../logos/lang/icons8-java.svg'),
            count: "",
            url: "projects/java"
        }, {    
            key: "sql",
            img: require('../logos/lang/sql.png'),
            count: 2,
            url: "projects/sql"
        },{    
            key: "bash",
            img: require('../logos/lang/bash.svg'),
            count: 3,
            url: "projects/bash"
        },{    
            key: "swift",
            img: require('../logos/lang/icons8-swift.svg'),
            count: 0,
            url: "projects/bash"
        }
    ],

    frame: [
        {    
            key: "wordpress",
            img: require('../logos/frame/icons8-wordpress.svg'),
            count: "",
            url: "projects/"
        },{    
            key: "laravel",
            img: require('../logos/frame/laravel-1-logo-svg-vector.svg'),
            count: "",
            url: "projects/"
        },{    
            key: "vuejs",
            img: require('../logos/frame/vuejs-seeklogo.com.svg'),
            count: "",
            url: "projects/"
        },{    
            key: "asp",
            img: require('../logos/frame/dot_net_core.png'),
            count: "",
            url: "projects/"
        },{    
            key: "bootstrap",
            img: require('../logos/frame/icons8-bootstrap.svg'),
            count: "",
            url: "projects/"
        }
    ],

    tech: [
        {    
            key: "rpi",
            img: require('../logos/tech/icons8-raspberry-pi.svg'),
            count: "",
            url: "projects/"
        },/*{    
            key: "git",
            img: require('../logos/tech/icons8-git.svg'),
            count: "",
            url: "projects/"
        },*/{    
            key: "debian",
            img: require('../logos/tech/debian.png'),
            count: "",
            url: "projects/"
        },{    
            key: "kali",
            img: require('../logos/tech/kali.png'),
            count: "",
            url: "projects/"
        },{    
            key: "centos",
            img: require('../logos/tech/centos.png'),
            count: "",
            url: "projects/"
        },{    
            key: "arduino",
            img: require('../logos/tech/arduino.png'),
            count: "",
            url: "projects/"
        },{    
            key: "aws",
            img: require('../logos/tech/aws.png'),
            count: "",
            url: "projects/"
        },{    
            key: "cisco",
            img: require('../logos/tech/cisco.png'),
            count: "",
            url: "projects/"
        }
    ]
}