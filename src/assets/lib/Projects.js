export default {
    additional: [
        {
            title: "<abbr title='Technologies for Web Application'>TWA</abbr>",
            desc: "<p>This project was to build an eCommerce site with w3 valid HTML 5, vanilla JavaScript, CSS3 and \
            <abbr title='Personal Home Page Hypertext Preprocessor'>PHP</abbr>. User input validation was to be \
            implemented on both client and serverside. The site was to have 3 different user types; Public, Member, \
            Staff.</p><p>Public users are able to view products and checkout as a guest, while members can review search \
            histories and save information for a faster checkout. Staff users are able to see all orders, members, \
            products and the associated details. The aim of the project is to explore, learn and familiarise with common \
            scenarios in web development.</p>",
            img: "",
            badges: [
                require('../logos/lang/icons8-javascript.svg'),
                require('../logos/lang/php-svg-to-png-3.png'),
                require('../logos/lang/icons8-css3.svg'),
                require('../logos/lang/icons8-html-5.svg'),
            ]
        },
        {
            title: "<abbr title='Stratti Building Group'>SBG</abbr> Gmail Script", 
            desc: "<p>The purpose of this project was to automate mailouts required by <abbr title='Stratti Building Group'>\
            SBG</abbr>. A python script was built to automatically attach weekly payslips, insert a breif personalised \
            message and send it out to the required recipients.</p><p>The python script interacts with the Gmail \
            <abbr title='Application Program Interface'>API</abbr> for creating and sending out the emails, as well as \
            a local installation of a DropBox directory for attaching company documents.</p>",
            img: "",
            badges: [
                require('../logos/lang/icons8-python.svg'),
                require('../logos/api/2000px-Gmail_Icon.svg.png'),
            ]
        },
        {
            title: "C++ Feistel & <abbr title='Rivest, Shamir, and Adelman'>R.S.A</abbr> Concept",
            desc: "<p>This project explored modern cryptography and bitwise minipulation through implementing a version of \
            the <abbr title='Data Encryption Standard'>DES</abbr> algorithm in C++ with a 20-bit key and 16-bit block of \
            cipher and plain-text. Additionally the project explored RSA encryption and modern prime factorial algorithms</p>",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/icons8-c++.svg')
            ]
        },
        {
            title: "C++ Mini-SHRDLU Game", /*Note Pdf api used, payment Stripe api, Google address api for autfill and validation */
            desc: "<p>Mini-SHRDLU Game was implemented in C++ to explore the use of heuristics and priority queues to implement a level of machine decision making in artificial intelligence</p>",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/icons8-c++.svg'),
            ]
        },/*{
            title: "C++ Ideas Bank", 
            desc: "Ideas Bank with a hash table and b-tree implementation of 'inverted indexing'",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/icons8-c++.svg'),
            ]
        },{
            title: "R Analysis of Seth Rogans Tweets", 
            desc: "Ideas Bank with a hash table and b-tree implementation of 'inverted indexing'",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/724px-R_logo.svg.png'),
            ]
        },*/{
            title: "Backup MySql", /*Note Pdf api used, payment Stripe api, Google address api for autfill and validation */
            desc: "<p>THis project developedd a script written in BASH to backup MySQL databases, the script was implemented through a cron job to automate the process</p>",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/bash.svg'),
            ]
        },
        /*{
            title: "BASH Scanning Ports", / *Note Pdf api used, payment Stripe api, Google address api for autfill and validation * /
            desc: "Ideas Bank with a hash table and b-tree implementation of 'inverted indexing'",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/bash.svg'),
            ]
        },
        {
            title: "BASH Script", / *Note Pdf api used, payment Stripe api, Google address api for autfill and validation * /
            desc: "Ideas Bank with a hash table and b-tree implementation of 'inverted indexing'",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/bash.svg'),
            ]
        },

        {
            title: "Java Projects", 
            desc: "Ideas Bank with a hash table and b-tree implementation of 'inverted indexing'",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/icons8-java.svg'),
            ]
        },


        {
            title: "C project", 
            desc: "Ideas Bank with a hash table and b-tree implementation of 'inverted indexing'",
            img: require("../projects/wwo-site.png"),
            badges: [
                require('../logos/lang/icons8-c-programming.svg'),
            ]
        },*/


    ],

    dsProjects: [
        {
            title: "Asthma Risk Detection",
            desc: "<p>The goal of this project was to apply and assess the performance of \
            various supervised machine learning algorithms in order to determine the relevant environmental \
            facotors likely to elevate a patients risk of an asthma attack. The environmental data used was sourced from \
            relevant weather and pollution station in Victoria, Autralia in relation to the patients. Whether or not a \
            patient suffered an asthma attack was also provided.</p> \
            <p>The project reveald that the relative humidity, Ozone (O3) and Nitrogen Dioxide (NO2) levels were the \
            most relevant environmental factors. These factors were used in logistical regression, decision trees and \
            support vector machines to create the best preforming model for predicting an asthma attack</p> \
            <p>Logistic Regression preformed the best our of all the models with an accuracy of 81% \
            on the testing datatset, providing evidence of a linear relationship.</p>",
            //"A supervised machine learning exercise to determine if environmental factors could be used as identifiers to predict if a patient is likely to suffer from an asthma attack or not",
            img: require("../projects/asthma-risk-detection.png"),
            badges: [
                require('../logos/lang/724px-R_logo.svg.png'),
                require('../logos/tech/tidyverse-ggplot2.png')
            ]
        },{
            title: "Urban Heat Islands",
            desc: "<p>The goal of this project was to apply and assess the performance of \
            various supervised machine learning algorithms in order to determine the relevant environmental \
            facotors likely to elevate a patients risk of an asthma attack. The environmental data used was sourced from \
            relevant weather and pollution station in Victoria, Autralia in relation to the patients. Whether or not a \
            patient suffered an asthma attack was also provided.</p> \
            <p>The project reveald that the relative humidity, Ozone (O3) and Nitrogen Dioxide (NO2) levels were the \
            most relevant environmental factors. These factors were used in logistical regression, decision trees and \
            support vector machines to create the best preforming model for predicting an asthma attack</p> \
            <p>Logistic Regression preformed the best our of all the models with an accuracy of 81% \
            on the testing datatset, providing evidence of a linear relationship.</p>",
            img: require("../projects/asthma-risk-detection.png"),
            badges: [
                require('../logos/lang/724px-R_logo.svg.png'),
                require('../logos/tech/tidyverse-ggplot2.png')
            ]
        },{
            title: "AMSI",
            desc: "<p>The goal of this project was to apply and assess the performance of \
            various supervised machine learning algorithms in order to determine the relevant environmental \
            facotors likely to elevate a patients risk of an asthma attack. The environmental data used was sourced from \
            relevant weather and pollution station in Victoria, Autralia in relation to the patients. Whether or not a \
            patient suffered an asthma attack was also provided.</p> \
            <p>The project reveald that the relative humidity, Ozone (O3) and Nitrogen Dioxide (NO2) levels were the \
            most relevant environmental factors. These factors were used in logistical regression, decision trees and \
            support vector machines to create the best preforming model for predicting an asthma attack</p> \
            <p>Logistic Regression preformed the best our of all the models with an accuracy of 81% \
            on the testing datatset, providing evidence of a linear relationship.</p>",
            img: require("../projects/asthma-risk-detection.png"),
            badges: [
                require('../logos/lang/724px-R_logo.svg.png'),
                require('../logos/tech/tidyverse-ggplot2.png')
            ]
        },{
            title: "Visual Analytics",
            desc: "<p>The goal of this project was to apply and assess the performance of \
            various supervised machine learning algorithms in order to determine the relevant environmental \
            facotors likely to elevate a patients risk of an asthma attack. The environmental data used was sourced from \
            relevant weather and pollution station in Victoria, Autralia in relation to the patients. Whether or not a \
            patient suffered an asthma attack was also provided.</p> \
            <p>The project reveald that the relative humidity, Ozone (O3) and Nitrogen Dioxide (NO2) levels were the \
            most relevant environmental factors. These factors were used in logistical regression, decision trees and \
            support vector machines to create the best preforming model for predicting an asthma attack</p> \
            <p>Logistic Regression preformed the best our of all the models with an accuracy of 81% \
            on the testing datatset, providing evidence of a linear relationship.</p>",
            img: require("../projects/asthma-risk-detection.png"),
            badges: [
                require('../logos/lang/724px-R_logo.svg.png'),
                require('../logos/tech/tidyverse-ggplot2.png')
            ]
        }/*,{
            title: "Application of Big Data",
            desc: "<p>The goal of this project was to apply and assess the performance of \
            various supervised machine learning algorithms in order to determine the relevant environmental \
            facotors likely to elevate a patients risk of an asthma attack. The environmental data used was sourced from \
            relevant weather and pollution station in Victoria, Autralia in relation to the patients. Whether or not a \
            patient suffered an asthma attack was also provided.</p> \
            <p>The project reveald that the relative humidity, Ozone (O3) and Nitrogen Dioxide (NO2) levels were the \
            most relevant environmental factors. These factors were used in logistical regression, decision trees and \
            support vector machines to create the best preforming model for predicting an asthma attack</p> \
            <p>Logistic Regression preformed the best our of all the models with an accuracy of 81% \
            on the testing datatset, providing evidence of a linear relationship.</p>",
            img: require("../projects/asthma-risk-detection.png"),
            badges: [
                require('../logos/lang/724px-R_logo.svg.png'),
                require('../logos/tech/tidyverse-ggplot2.png')
            ]
        },*/
    ],

    projects: [
        {
            title: "<abbr title='Health Information Staging System'>HISS</abbr>",
            desc: "<p>This project developed a framework for a Health Information Survey System (HISS) to deliver interventive content. \
            The site was developed with a decoupled architecure as a Vue JS <abbr title='Single Page Application'>SPA</abbr> \
            frontend that consumed a Laravel RESTful API.</p><p>This project additionaly integrated with the JS libraries \
            Chart JS, Quill JS and JSPlumb and was deployed on a Linux CentOS Virtual Machine</p>",
            img: require("../projects/hiss-site.png"),
            badges: [
                
                require('../logos/frame/laravel-1-logo-svg-vector.svg'),
                require('../logos/frame/vuejs-seeklogo.com.svg'),
                require('../logos/frame/icons8-bootstrap.svg'),
            ]
        },{
            title: "Stratti Building",
            desc: "<p>This project developed a site from scratch for a sydney building firm. All of the written content \
            had to be created from scratch, creative control was also given to the design and layout of the site. In \
            order to enable the firm self-management of future content and media updates, the site was deployed using \
            the Wordpress content management system (CMS).</p>",
            img: require("../projects/sbg-site.png"),
            badges: [
                require('../logos/lang/php-svg-to-png-3.png'),
                require('../logos/frame/icons8-wordpress.svg')
            ]
        },{
            title: "Workwear One", /*Note Pdf api used, payment Stripe api, Google address api for autfill and validation */
            desc: "<p>This project developed an eCommerce site that allowed the customisation of it's products. The project \
            was developed in raw PHP with a JSON API exposed to a vanilla Javascript and HTML frontend.</p><p>This project \
            additionaly integrated with the payment gateway Stripe and leveraged PHP libraries such as imagemagick, FPDF and the Google Address API</p>",
            img: require("../projects/wwo-site.png"),
            badges: [
                
                require('../logos/lang/php-svg-to-png-3.png'),
                require('../logos/lang/icons8-javascript.svg'),
                require('../logos/lang/icons8-css3.svg'),
                require('../logos/lang/icons8-html-5.svg'),
            ]
        }/*,{
            title: "Standen Celebrants",
            desc: "<p>This project upgraded an existing website for a celebrant from an out date brochure to a modern responsive site. \
            The Wordpress content management system (CMS) was utilised to allow the owners of the site to self-manage content and pages.</p>",
            img: require("../projects/standen-site.png"),
            badges: [
                require('../logos/lang/php-svg-to-png-3.png'),
                require('../logos/frame/icons8-wordpress.svg')
            ]
        }/*,{
            title: "<abbr title='Human & Computer Interaction'>HCI</abbr>",
            desc: "VueJS: Rapid prototype of a daily diary client side application explores best design practices and priciples JavaScript",
            img: require("../projects/wwo-site.png"),
            badges: [

                require('../logos/frame/vuejs-seeklogo.com.svg'),
            ]
        },{
            title: "SBG Invoice Tracking",
            desc: "<p>This project developed a Python script to pull all invoices from email attachments using the GMAIL \
            API, the Python library invoice2data was then utilised to scrape vital data and update a Google sheet using \
            the SHEETS API. Python was also used to search and compare files in a local DropBox repository to help track \
            paid invoices and update the google sheet acordingly.</p>",
            img: require("../projects/SBG_script.png"),
            badges: [
                require('../logos/lang/icons8-python.svg'),
                require('../logos/tech/google-sheets.png'),
                require('../logos/tech/Gmail.png'),
            ]
        },*/,{
            title: "Room Viewer WSU",
            desc: "<p>The Room Viewer service will be displayed on tablets outside of rooms across Western Sydney University \
            Vertical Campuses. Students and staff can view a room’s avilibility for the day, the current booking status \
            and scan a QR code which directs the device scanning towards the recourse booker</p> <h4>Features include</h4> \
            <ul><li>Scrollable 24 hour schedule of bookings</li> \
            <li>Booking status, time and date display</li> \
            <li>Scannable QR codes for applicable rooms</li></ul><p>The service leverages an existing API provided by WSU Campus Navigator system for room availabilities. \
            The project was initiated to avoid third party vendor lock-in and increase the maintainability of the \
            service. This was achived by moving from the deprecated Angular JS to Angular 2 (v7.4), supplying well \
            documented code and centralising resources</p>",
            img: require("../projects/room-viewer.png"),
            badges: [
                require('../logos/frame/angular.png'),
                require('../logos/frame/laravel-1-logo-svg-vector.svg'),
            ]
        },{
            title: "Staff Directory WSU",
            desc: "<p>The Room Viewer service will be displayed on tablets outside of rooms across Western Sydney University \
            Vertical Campuses. Students and staff can view a room’s avilibility for the day, the current booking status \
            and scan a QR code which directs the device scanning towards the recourse booker</p> <h4>Features include</h4> \
            <ul><li>Scrollable 24 hour schedule of bookings</li> \
            <li>Booking status, time and date display</li> \
            <li>Scannable QR codes for applicable rooms</li></ul><p>The service leverages an existing API provided by WSU Campus Navigator system for room availabilities. \
            The project was initiated to avoid third party vendor lock-in and increase the maintainability of the \
            service. This was achived by moving from the deprecated Angular JS to Angular 2 (v7.4), supplying well \
            documented code and centralising resources</p>",
            img: require("../projects/room-viewer.png"),
            badges: [
                require('../logos/frame/angular.png'),
                require('../logos/frame/laravel-1-logo-svg-vector.svg'),
            ]
        },{
            title: "Campus Navigator WSU",
            desc: "<p>The Room Viewer service will be displayed on tablets outside of rooms across Western Sydney University \
            Vertical Campuses. Students and staff can view a room’s avilibility for the day, the current booking status \
            and scan a QR code which directs the device scanning towards the recourse booker</p> <h4>Features include</h4> \
            <ul><li>Scrollable 24 hour schedule of bookings</li> \
            <li>Booking status, time and date display</li> \
            <li>Scannable QR codes for applicable rooms</li></ul><p>The service leverages an existing API provided by WSU Campus Navigator system for room availabilities. \
            The project was initiated to avoid third party vendor lock-in and increase the maintainability of the \
            service. This was achived by moving from the deprecated Angular JS to Angular 2 (v7.4), supplying well \
            documented code and centralising resources</p>",
            img: require("../projects/room-viewer.png"),
            badges: [
                require('../logos/frame/angular.png'),
                require('../logos/frame/laravel-1-logo-svg-vector.svg'),
            ]
        },/*{
            title: "Western Hotel",
            desc: "<p>This project developed a fictious hotel site where customers could book in dates and view \
            availabilities of hotel rooms. The site was developed in ASP.Net Core and utilised the Bootstrap design \
            framework. This project was developed in a group of 3 with my duties covering the booking system</p>",
            img: require("../projects/wsu_hotel.png"),
            badges: [
                require('../logos/lang/icons8-c-sharp-logo.svg'),
                require('../logos/frame/dot_net_core.png'),
                require('../logos/frame/icons8-bootstrap.svg')
            ]
        },*/{
            title: "Portfolio",
            desc: "<p>This project developed as a single page application in the frontend framework Vue JS. The project \
            was developed to showcase my prior experience and ability by developing all components, CSS and transitions. \
            The site is deployed using a serveless architecture as a static site served from an amazon S3 bucket to \
            minimise operational costs</p>", 
            img: require("../projects/resume-site.png"),
            badges: [
                require('../logos/frame/vuejs-seeklogo.com.svg'),
            ]
        },/*{
            title: "StudyBudy",
            desc: "VueJS: Rapid prototype of a daily diary client side application explores best design practices and priciples JavaScript",
            img: require("../projects/studybudy.png"),
            badges: [
                require('../logos/frame/vuejs-seeklogo.com.svg'),
                require('../logos/frame/vuetify-seeklogo.com.svg')
            ]
        },*/{
            title: "Paramed360 Interactive",
            desc: "<p>The project is currently being developed by a group of 3, to facilitate Western Sydney University \
            (WSU) School of Science and Heath experiential learning by expanding the functionality of their current \
            systems.</p><p>The project involves 3 modular microservices that interact with the existing infrastructure, \
            the first is a user-friendly <abbr title='Single Page Application'>SPA</abbr> developed in Angular to \
            streamline the management of immersive media and related metadata.</p><p>The second is a RESTful API developed in \
            Node.js and the Express framework to interact with the current media server and facilitate uploads.</p><p>\
            The third is a service to interact with the Google Photos API and the RESTful API we develop, in order to \
            automate media imports that are uploaded to a specifc folder.</p>",
            img: require("../projects/Paramed360_network.png"),
            badges: [
                require('../logos/frame/angular.png'),
                require('../logos/frame/nodejs.png'),
                require('../logos/frame/expressjs.png'),
            ]
        },
    ]
}