# Portfolio Docker build 
FROM node:lts-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 8080
CMD npm install && npm run serve

# docker build . -t ds-cv
# docker run -it -v /Users/daniel/Sites/portfolio/portfolio:/app -p 8080:8080 ds-cv npm run serve